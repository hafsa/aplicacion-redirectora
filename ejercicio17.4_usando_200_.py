#!/usr/bin/python

#
# Simple HTTP Server Random
# Jesus M. Gonzalez-Barahona
# jgb @ gsyc.es
# TSAI and SAT subjects (Universidad Rey Juan Carlos)
# September 2009
# Febraury 2022
#
# Returns an HTML page with a random link

import socket
import random

# Create a TCP objet socket and bind it to a port
# We bind to 'localhost', therefore only accepts connections from the
# same machine
# Port should be 80, but since it needs root privileges,
# let's use one above 1024

myPort = 1239
mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
mySocket.bind(('localhost', myPort))

# Queue a maximum of 10 TCP connection requests

mySocket.listen(5)

# Initialize random number generator (not exactly needed, since the
# import of the module already is supposed to do that). No argument
# means time is used as seed (or OS supplied random seed).
random.seed()

# Accept connections, read incoming data, and answer back an HTLM page
#  (in a loop)
try:
	while True:
		print("Waiting for connections")
		(recvSocket, address) = mySocket.accept()
		print("HTTP request received:")
		print(recvSocket.recv(2048))

		# Resource name for next url
		listaurls = ['https://gitlab.etsit.urjc.es/cursosweb/2022-2023', 'https://www.aulavirtual.urjc.es',
					 'https://www.zara.com/es/', 'https://github.com/CursosWeb/Code/tree/master/Python-Web',
					 'https://news.google.com/home?hl=es&gl=ES&ceid=ES:es']
		Url = str (random.choice (listaurls))
		nextUrl = Url
		# HTML body of the page to serve
		htmlBody = "<h1>Welcome!</h1>" + '<p>Your randomly selected page is: <a href="' \
		    + nextUrl + '">' + nextUrl + "</a></p>"
		recvSocket.send(b"HTTP/1.1 200 OK \r\n\r\n" +
				b"<html><body>" + htmlBody.encode('ascii') + b"</body></html>" +
				b"\r\n")
		recvSocket.close()

except KeyboardInterrupt:
	print("Closing binded socket")
	mySocket.close()